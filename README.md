# git_initiation

This project is an introduction to the functionality of git.

| Commande | Description/Comments |
| ------ | ------ |
| git clone https://gitlab.com/antoine.lamer/git_initiation/ | Cloner le répertoire git_iniation|
| ls | list directory contents | 
| cd git_initiation | Entrer dans le répertoire git_initiation |
| ls |  |
| git status | Afficher la liste des fichiers modifiés, validés ou déjà suivis |
| git checkout -b my_branch | Créer la branche my_branch |
|  | Edit some_code.R in your local repository |
| git status |     |
| git add some_code.R | Add the file some_code.R to the staging area (index)   |
| git status |     |
| git commit -m 'Add one record' |    |
| git status |     |
| git checkout master |    |
| git branch -v |    |
| git merge my_branch |   |
| git branch -v |    |
| git branch -d my_branch |   |

# Useful articles and tutorials

[learn-enough-git-to-be-useful](https://towardsdatascience.com/learn-enough-git-to-be-useful-281561eef959) <br />
[getting-started-with-git-and-github](https://towardsdatascience.com/getting-started-with-git-and-github-6fcd0f2d4ac6)